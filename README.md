# subsonic_dart

## What is subsonic_dart
subsonic_dart is a library for interacting with a 
[Subsonic compatible API](http://www.subsonic.org).
This library will only support the audio portion of
subsonic. There are no plans to support video functionality.

## API Coverage
| API                        |  |
|----------------------------|--|
| ping                       |✅|
| getMusicFolders            |✅|
| getGenres                  |✅|
| getArtists                 |✅|
| getArtist                  |✅|
| getAlbum                   |✅|
| getSong                    |✅|
| getArtistInfo              |✅|
| getSimilarSongs            |✅|
| getTopSongs                |✅|
| getAlbumList               |✅|
| getRandomSongs             |✅|
| getSongsByGenre            |✅|
| getStarred                 |✅|
| search                     |✅|
| getPlaylists               |✅|
| getPlaylist                |✅|
| createPlaylist             |✅|
| updatePlaylist             |✅|
| deletePlaylist             |✅|
| stream                     |✅|
| download                   |✅|
| getCoverArt                |✅|
| getLyrics                  |✅|
| getAvatar                  |✅|
| star                       |✅|
| unstar                     |✅|
| setRating                  |  |
| scrobble                   |  |
| getUser                    |✅|
| getBookmarks               |  |
| createBookmark             |  |
| deleteBookmark             |  |
| getScanStatus              |  |
| startScan                  |  |
