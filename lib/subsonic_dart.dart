/// Dart library that provides a Client to interact with the Subsonic API
library;

export "src/client.dart";
export "src/models/album.dart";
export "src/models/artist.dart";
export "src/models/search.dart";
export "src/models/playable.dart";
export "src/models/song.dart";
export "src/models/playlist.dart";
export "src/models/starred.dart";
export "src/models/genre.dart";
