import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:http/http.dart';
import 'package:subsonic_dart/src/errors/subsonic_error.dart';
import 'package:subsonic_dart/src/models/album.dart';
import 'package:subsonic_dart/src/models/artist.dart';
import 'package:subsonic_dart/src/models/genre.dart';
import 'package:subsonic_dart/src/models/music_folder.dart';
import 'package:subsonic_dart/src/models/playlist.dart';
import 'package:subsonic_dart/src/models/search.dart';
import 'package:subsonic_dart/src/models/song.dart';
import 'package:subsonic_dart/src/models/starred.dart';
import 'package:subsonic_dart/src/models/user.dart';
import 'package:uuid/v4.dart';

const String version = "1.16.1";

class BitRate {
  final int _bitsPerSecond;

  int get kbps {
    return (_bitsPerSecond / 1000).round();
  }

  int get bps {
    return _bitsPerSecond;
  }

  const BitRate({int bits = 0, int kiloBits = 0})
      : _bitsPerSecond = (bits) + (kiloBits * 1000);
}

enum URIFormat {
  https,
  http;
}

enum AlbumListType {
  random("random"),
  newest("newest"),
  highest("highest"),
  frequent("frequent"),
  recent("recent"),
  alphabeticalByName("alphabeticalByName"),
  alphabeticalByArtist("alphabeticalByArtist"),
  starred("starred"),
  byYear("byYear"),
  byGenre("byGenre");

  final String _name;

  const AlbumListType(this._name);

  @override
  toString() {
    return _name;
  }
}

class SubSonicClient {
  String hostname;
  String username;
  String token;
  String salt;
  URIFormat uriFormat;

  SubSonicClient(this.hostname, this.username, this.token, this.salt,
      {this.uriFormat = URIFormat.https});

  Map<String, String> getBaseQueryParams() => {
        "u": username,
        "t": token,
        "s": salt,
        "v": version,
        "f": "json",
        "c": "subsonic_dart"
      };

  factory SubSonicClient.withAuth(
      String hostname, String username, String password,
      {URIFormat? uriFormat}) {
    final salt = UuidV4().generate();

    final token = md5.convert(utf8.encode(password + salt));

    return SubSonicClient(hostname, username, token.toString(), salt,
        uriFormat: uriFormat ?? URIFormat.https);
  }

  static Future<bool> testCredentials(
      String hostname, String username, String password,
      {URIFormat? uriFormat}) async {
    final client = SubSonicClient.withAuth(hostname, username, password,
        uriFormat: uriFormat);

    return await client.ping();
  }

  Uri getUri(String path, Map<String, String>? query) {
    final baseQuery = getBaseQueryParams();
    if (query != null) baseQuery.addAll(query);

    return switch (uriFormat) {
      URIFormat.https => Uri.https(hostname, "/rest$path", baseQuery),
      URIFormat.http => Uri.http(hostname, "/rest$path", baseQuery)
    };
  }

  Future<Map<String, dynamic>> _sendRequest(String path,
      {Map<String, String>? query}) async {
    final Map<String, String> queryParams = {};

    if (query != null) {
      queryParams.addAll(query);
    }

    final request = Request("GET", getUri(path, queryParams));

    final response = await request.send();

    if (response.statusCode >= 300) {
      throw SubSonicException(await response.stream.bytesToString());
    }

    final body = jsonDecode(await response.stream.bytesToString());

    final subsonicBody = body["subsonic-response"];

    if (subsonicBody["status"] != "ok") {
      throw SubSonicException(subsonicBody["error"]["message"]);
    }

    return subsonicBody;
  }

  // System

  /// Pings the Navidrome server, returns true on a successful ping
  Future<bool> ping() async {
    try {
      await _sendRequest("/ping");
    } catch (err) {
      return false;
    }
    return true;
  }

  //Browsing

  Future<List<MusicFolder>> getMusicFolders() async {
    final response = await _sendRequest("/getMusicFolders");

    return (response["musicFolders"]["musicFolder"] as List<dynamic>)
        .map((data) => MusicFolder.fromJSON(data))
        .toList();
  }

  Future<Song> getSong(String songId) async {
    final response = await _sendRequest("/getSong", query: {"id": songId});

    return Song.fromJSON(this, response["song"]);
  }

  Future<List<Artist>> getArtists() async {
    final response = await _sendRequest("/getArtists");

    return (response["artists"]["index"] as List<dynamic>)
        .map((data) => data["artist"] as List<dynamic>)
        .expand((element) => element)
        .map((data) => Artist.fromJSON(this, data))
        .toList();
  }

  Future<Artist> getArtist(String id) async {
    final response = await _sendRequest("/getArtist", query: {"id": id});
    return Artist.fromJSON(this, response["artist"]);
  }

  Future<Album> getAlbum(String albumId) async {
    final response = await _sendRequest("/getAlbum", query: {"id": albumId});

    return Album.fromJSON(this, response["album"]);
  }

  Future<List<Genre>> getGenres() async {
    final response = await _sendRequest("/getGenres");

    return (response["genres"]["genre"] as List<dynamic>?)
            ?.map((data) => Genre.fromJSON(data))
            .toList() ??
        [];
  }

  Future<List<Song>> getTopSongs(String artistName) async {
    final response =
        await _sendRequest("/getTopSongs", query: {"artist": artistName});

    return (response["topSongs"]["song"] as List<dynamic>)
        .map((data) => Song.fromJSON(this, data))
        .toList();
  }

  Future<List<Song>> getSimilarSongs(String songId) async {
    final response =
        await _sendRequest("/getSimilarSongs", query: {"id": songId});

    return (response["similarSongs"]["song"] as List<dynamic>)
        .map((data) => Song.fromJSON(this, data))
        .toList();
  }

  // Album/Songs Lists

  Future<AlbumList> getAlbums(
      {AlbumListType type = AlbumListType.alphabeticalByName,
      count = 20,
      offset = 0}) async {
    final response = await _sendRequest("/getAlbumList", query: {
      "type": type.toString(),
      "offset": offset.toString(),
      "size": count.toString()
    });

    return AlbumList.fromJSON(
        this, type, count, response["albumList"]["album"] as List<dynamic>?);
  }

  Future<Starred> getStarred() async {
    final response = await _sendRequest("/getStarred");

    return Starred.fromJSON(this, response["starred"]);
  }

  Future<List<Song>> getRandomSongs() async {
    final response = await _sendRequest("/getRandomSongs");

    return (response["randomSongs"]["song"] as List<dynamic>)
        .map((data) => Song.fromJSON(this, data))
        .toList();
  }

  Future<SongsByGenre> getSongsByGenre(String genre,
      {int count = 20, int offset = 0}) async {
    final response = await _sendRequest("/getSongsByGenre", query: {
      "genre": genre,
      "count": count.toString(),
      "offset": offset.toString()
    });

    return SongsByGenre.fromJSON(
        this, genre, count, response["songsByGenre"]["song"]);
  }

  // Search

  Future<SearchResults> search(
    String query, {
    offsetArtists = 0,
    offsetAlbums = 0,
    offsetSongs = 0,
    count = 20,
  }) async {
    final response = await _sendRequest("/search2", query: {
      "query": query,
      "artistCount": count.toString(),
      "artistOffset": offsetArtists.toString(),
      "albumCount": count.toString(),
      "albumOffset": offsetAlbums.toString(),
      "songCount": count.toString(),
      "songOffset": offsetSongs.toString()
    });
    return SearchResults.fromJSON(
        this, query, count, response["searchResult2"]);
  }

  // Playlists

  Future<List<Playlist>> getPlaylists() async {
    final response = await _sendRequest("/getPlaylists", query: {});

    return (response["playlists"]["playlist"] as List<dynamic>?)
            ?.map((data) => Playlist.fromJSON(this, data))
            .toList() ??
        [];
  }

  Future<Playlist> getPlaylist(String id) async {
    final response = await _sendRequest("/getPlaylist", query: {"id": id});
    return Playlist.fromJSON(this, response["playlist"]);
  }

  Future<Playlist> createPlaylist(String name) async {
    final response =
        await _sendRequest("/createPlaylist", query: {"name": name});

    return Playlist.fromJSON(this, response["playlist"]);
  }

  Future updatePlaylist(String id,
      {String? name,
      String? comment,
      bool? public,
      String? songIdToAdd,
      int? songIndexToRemove}) async {
    final query = {"playlistId": id};

    if (name != null) {
      query.addAll({"name": name});
    }
    if (comment != null) {
      query.addAll({"comment": comment});
    }
    if (public != null) {
      query.addAll({"public": public.toString()});
    }
    if (songIdToAdd != null) {
      query.addAll({"songIdToAdd": songIdToAdd});
    }
    if (songIndexToRemove != null) {
      query.addAll({"songIndexToRemove": songIndexToRemove.toString()});
    }

    await _sendRequest("/updatePlaylist", query: query);
  }

  Future deletePlaylist(String id) async {
    await _sendRequest("/deletePlaylist", query: {"id": id});
  }

  // Media Retrieval

  Uri getSongStreamUri(String playableId,
      {BitRate maxBitrate = const BitRate(bits: 0), String format = "raw"}) {
    return getUri("/stream", {
      "id": playableId,
      "maxBitRate": maxBitrate.kbps.toString(),
      "format": format
    });
  }

  Uri getSongDownloadUri(String playableId) {
    return getUri("/download", {"id": playableId});
  }

  Uri getCoverArtUri(String playableId, {int size = 256}) {
    return getUri("/getCoverArt", {"id": playableId, "size": size.toString()});
  }

  Future<String> getLyrics(String artist, String title) async {
    final response = await _sendRequest("/getLyrics",
        query: {"artist": artist, "title": title});

    return response["lyrics"]["value"];
  }

  Uri getAvatarUri() {
    return getUri("/getAvatar", {});
  }

  // Media Annotation

  Future star(String id) async {
    await _sendRequest("/star", query: {"id": id});
  }

  Future unstar(String id) async {
    await _sendRequest("/unstar", query: {"id": id});
  }

  // User Management

  Future<User> getUser() async {
    final response = await _sendRequest("/getUser");

    return User.fromJSON(response["user"]);
  }

  // etc

  Future<List<Album>> getArtistAlbums(String id) async {
    final response = await _sendRequest("/getArtist", query: {"id": id});
    return (response["artist"]["album"] as List<dynamic>?)
            ?.map((data) => Album.fromJSON(this, data))
            .toList() ??
        [];
  }

  Future<List<Song>> getAlbumSongs(String albumId) async {
    final response = await _sendRequest("/getAlbum", query: {"id": albumId});

    return (response["album"]["song"] as List<dynamic>)
        .map((data) => Song.fromJSON(this, data))
        .toList();
  }

  Future<List<PlaylistEntry>> getPlaylistEntries(String id) async {
    final response = await _sendRequest("/getPlaylist", query: {"id": id});
    return (response["playlist"]["entry"] as List<dynamic>?)
            ?.map((e) => PlaylistEntry.fromJSON(this, e))
            .toList() ??
        [];
  }
}
