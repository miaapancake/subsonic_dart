class SubSonicException implements Exception {
  String message;

  SubSonicException(this.message);

  @override
  String toString() {
    return message;
  }
}
