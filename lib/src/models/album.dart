import 'package:subsonic_dart/src/client.dart';
import 'package:subsonic_dart/src/models/artist.dart';
import 'package:subsonic_dart/src/models/song.dart';

class Album {
  SubSonicClient _client;

  String id;
  String? parent;
  String name;
  String artist;
  String artistId;
  int? year;
  String? genre;
  String coverArt;
  Duration duration;
  DateTime created;
  int songCount;

  List<Song>? songs;

  Album(
      this._client,
      this.id,
      this.parent,
      this.name,
      this.artist,
      this.artistId,
      this.year,
      this.genre,
      this.coverArt,
      this.duration,
      this.created,
      this.songCount,
      this.songs);

  factory Album.fromJSON(SubSonicClient client, Map<String, dynamic> data) {
    return Album(
        client,
        data["id"],
        data["parent"],
        data["name"],
        data["artist"],
        data["artistId"],
        data["year"] as int?,
        data["genre"],
        data["coverArt"],
        Duration(seconds: data["duration"] as int),
        DateTime.parse(data["created"]),
        data["songCount"] as int,
        (data["song"] as List<dynamic>?)
            ?.map((data) => Song.fromJSON(client, data))
            .toList());
  }

  Uri getCoverArtUri({int size = 256}) {
    return _client.getCoverArtUri(id, size: size);
  }

  Future<Artist> getArtist() async {
    return await _client.getArtist(artistId);
  }

  Future<List<Song>> getSongs() async {
    if (songs != null) return songs!;
    return await _client.getAlbumSongs(id);
  }

  @override
  String toString() {
    return "$name by $artist";
  }
}

class AlbumList {
  int _offset = 0;
  final AlbumListType _type;

  final int _count;

  final SubSonicClient _client;
  final List<Album> albums;

  AlbumList(this._client, this._type, this._count, this.albums);

  factory AlbumList.fromJSON(SubSonicClient client, AlbumListType type,
      int count, List<dynamic>? data) {
    return AlbumList(client, type, count,
        data?.map((data) => Album.fromJSON(client, data)).toList() ?? []);
  }

  Future<bool> fetchMore() async {
    _offset += _count;
    final result =
        await _client.getAlbums(type: _type, offset: _offset, count: _count);

    if (result.albums.isNotEmpty) {
      albums.addAll(result.albums);
      return true;
    }

    return false;
  }

  @override
  String toString() {
    return "${albums.length} albums";
  }
}
