import 'package:subsonic_dart/src/client.dart';
import 'package:subsonic_dart/src/models/album.dart';
import 'package:subsonic_dart/src/models/artist.dart';

class Playable {
  final SubSonicClient _client;
  final String id;
  final String title;
  final String album;
  final String artist;

  int track;

  final String? artistId;
  final String? albumId;

  final int _duration;

  Duration get duration => Duration(seconds: _duration);

  Uri getCoverArtUri({int size = 256}) {
    return _client.getCoverArtUri(id, size: size);
  }

  Uri getStreamUri(
      {BitRate maxBitrate = const BitRate(bits: 0), String format = "raw"}) {
    return _client.getSongStreamUri(id, maxBitrate: maxBitrate, format: format);
  }

  Uri getDownloadUri() {
    return _client.getSongDownloadUri(id);
  }

  Future<String> getLyrics() async {
    return await _client.getLyrics(artist, title);
  }

  Future<Album?> getAlbum() async {
    if (albumId != null) return null;
    return await _client.getAlbum(albumId!);
  }

  Future<Artist?> getArtist() async {
    if (artistId != null) return null;
    return await _client.getArtist(artistId!);
  }

  Playable(this._client, this.id, this.title, this.album, this.artist,
      this.albumId, this.artistId, this.track, this._duration);

  @override
  String toString() {
    return "$title by $artist (${duration.inMinutes}:${duration.inSeconds % 60})";
  }
}
