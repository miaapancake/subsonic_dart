import 'package:subsonic_dart/src/client.dart';
import 'package:subsonic_dart/src/models/album.dart';
import 'package:subsonic_dart/src/models/song.dart';

class Artist {
  SubSonicClient _client;

  String id;
  String name;
  int albumCount;
  String coverArt;
  String? imageURL;

  List<Album>? albums;

  Artist(this._client, this.id, this.name, this.albumCount, this.coverArt,
      this.imageURL, this.albums);

  factory Artist.fromJSON(SubSonicClient client, Map<String, dynamic> data) {
    return Artist(
        client,
        data["id"],
        data["name"],
        data["albumCount"],
        data["coverArt"],
        data["imageURL"],
        (data["album"] as List<dynamic>?)
            ?.map((data) => Album.fromJSON(client, data))
            .toList());
  }

  Uri getCoverArtUri({int size = 256}) {
    return _client.getCoverArtUri(id, size: size);
  }

  Future<List<Album>> getAlbums() async {
    if (albums != null) return albums!;
    return await _client.getArtistAlbums(id);
  }

  Future<List<Song>> getSongs() async {
    final albums = await getAlbums();
    List<Song> songs = List.empty(growable: true);
    for (final album in albums) {
      songs.addAll(await album.getSongs());
    }
    return songs;
  }

  @override
  toString() {
    return "$name($id)";
  }
}
