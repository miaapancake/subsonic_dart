import 'package:subsonic_dart/src/client.dart';
import 'package:subsonic_dart/src/models/album.dart';
import 'package:subsonic_dart/src/models/artist.dart';
import 'package:subsonic_dart/src/models/song.dart';

class SearchResults {
  SubSonicClient _client;
  String _query;
  int _count;
  int _albumOffset = 0;
  int _artistOffset = 0;
  int _songOffset = 0;

  List<Artist> artists;
  List<Song> songs;
  List<Album> albums;

  SearchResults(this._client, this._query, this._count, this.artists,
      this.songs, this.albums);

  factory SearchResults.fromJSON(SubSonicClient client, String query, int count,
      Map<String, dynamic> data) {
    return SearchResults(
      client,
      query,
      count,
      (data["artist"] as List<dynamic>?)
              ?.map((a) => Artist.fromJSON(client, a))
              .toList() ??
          [],
      (data["song"] as List<dynamic>?)
              ?.map((a) => Song.fromJSON(client, a))
              .toList() ??
          [],
      (data["album"] as List<dynamic>?)
              ?.map((a) => Album.fromJSON(client, a))
              .toList() ??
          [],
    );
  }

  Future<bool> fetchMoreArtists() async {
    _artistOffset += _count;
    final results = await _client.search(_query,
        offsetArtists: _artistOffset, count: _count);

    if (results.artists.isNotEmpty) {
      artists.addAll(results.artists);
      return true;
    }

    return false;
  }

  Future<bool> fetchMoreAlbums() async {
    _albumOffset += _count;
    final results =
        await _client.search(_query, offsetAlbums: _albumOffset, count: _count);

    if (results.albums.isNotEmpty) {
      albums.addAll(results.albums);
      return true;
    }

    return false;
  }

  Future<bool> fetchMoreSongs() async {
    _songOffset += _count;
    final results =
        await _client.search(_query, offsetSongs: _songOffset, count: _count);

    if (results.songs.isNotEmpty) {
      songs.addAll(results.songs);
      return true;
    }

    return false;
  }

  @override
  toString() {
    return "${artists.length} artists, ${songs.length} songs, ${albums.length} albums";
  }
}
