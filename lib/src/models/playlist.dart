import 'package:subsonic_dart/src/client.dart';
import 'package:subsonic_dart/src/models/playable.dart';

class Playlist {
  SubSonicClient _client;

  String id;
  String name;
  int songCount;
  Duration duration;
  bool public;
  String owner;
  DateTime created;
  DateTime changed;
  String coverArt;

  List<PlaylistEntry>? entries;

  Playlist(
      this._client,
      this.id,
      this.name,
      this.songCount,
      this.duration,
      this.public,
      this.owner,
      this.created,
      this.changed,
      this.coverArt,
      this.entries);

  factory Playlist.fromJSON(SubSonicClient client, Map<String, dynamic> data) {
    return Playlist(
        client,
        data["id"],
        data["name"],
        data["songCount"],
        Duration(seconds: data["duration"] as int),
        data["public"] as bool,
        data["owner"],
        DateTime.parse(data["created"]),
        DateTime.parse(data["changed"]),
        data["coverArt"],
        (data["entries"] as List<dynamic>?)
            ?.map((data) => PlaylistEntry.fromJSON(client, data))
            .toList());
  }

  Uri getCoverArtUri({int size = 256}) {
    return _client.getCoverArtUri(id, size: size);
  }

  Future<List<PlaylistEntry>> getEntries() async {
    if (entries != null) return entries!;
    return await _client.getPlaylistEntries(id);
  }

  Future update({String? name, String? comment, bool? public}) async {
    await _client.updatePlaylist(id,
        name: name, public: public, comment: comment);
  }

  Future removeSong(int songIndex) async {
    await _client.updatePlaylist(id, songIndexToRemove: songIndex);
  }

  Future addSong(String songId) async {
    await _client.updatePlaylist(id, songIdToAdd: songId);
  }

  @override
  String toString() {
    return "$name by $owner ($songCount songs)";
  }
}

class PlaylistEntry extends Playable {
  int discNumber;
  int? playCount;

  int bitRate;
  int size;

  DateTime? played;

  PlaylistEntry(
    super._client,
    super.id,
    super.title,
    super.album,
    super.artist,
    super.artistId,
    super.albumId,
    super.track,
    super._duration,
    this.discNumber,
    this.playCount,
    this.bitRate,
    this.size,
    this.played,
  );

  factory PlaylistEntry.fromJSON(
      SubSonicClient client, Map<String, dynamic> data) {
    return PlaylistEntry(
      client,
      data["id"],
      data["title"],
      data["album"],
      data["artist"],
      data["artistId"],
      data["albumId"],
      data["track"],
      data["duration"] as int,
      data["discNumber"],
      data["playCount"],
      data["bitRate"],
      data["size"],
      data["played"] != null ? DateTime.parse(data["played"]) : null,
    );
  }
}
