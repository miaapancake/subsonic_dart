import 'package:subsonic_dart/src/client.dart';
import 'package:subsonic_dart/src/models/album.dart';
import 'package:subsonic_dart/src/models/artist.dart';
import 'package:subsonic_dart/src/models/song.dart';

class Starred {
  final List<Artist> artists;
  final List<Song> songs;
  final List<Album> albums;

  const Starred(
    this.artists,
    this.songs,
    this.albums,
  );

  factory Starred.fromJSON(SubSonicClient client, Map<String, dynamic> data) {
    return Starred(
      (data["artist"] as List<dynamic>?)
              ?.map((a) => Artist.fromJSON(client, a))
              .toList() ??
          [],
      (data["song"] as List<dynamic>?)
              ?.map((a) => Song.fromJSON(client, a))
              .toList() ??
          [],
      (data["album"] as List<dynamic>?)
              ?.map((a) => Album.fromJSON(client, a))
              .toList() ??
          [],
    );
  }

  @override
  toString() {
    return "${artists.length} artists, ${songs.length} songs, ${albums.length} albums";
  }
}
