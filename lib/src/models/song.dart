import 'package:subsonic_dart/src/client.dart';
import 'package:subsonic_dart/src/models/playable.dart';

class Song extends Playable {
  String parent;
  int? year;
  String? genre;
  int size;
  String contentType;
  String suffix;
  int bitRate;
  String path;
  int? discNumber;
  DateTime created;
  String type;

  Song(
    super._client,
    super.id,
    super.title,
    super.album,
    super.artist,
    super.albumId,
    super.artistId,
    super.track,
    super._duration,
    this.parent,
    this.year,
    this.genre,
    this.size,
    this.contentType,
    this.suffix,
    this.bitRate,
    this.path,
    this.discNumber,
    this.created,
    this.type,
  );

  factory Song.fromJSON(SubSonicClient client, Map<String, dynamic> data) {
    return Song(
      client,
      data["id"],
      data["title"],
      data["album"],
      data["artist"],
      data["albumId"],
      data["artistId"],
      data["track"] as int,
      data["duration"] as int,
      data["parent"],
      data["year"] as int?,
      data["genre"],
      data["size"],
      data["contentType"],
      data["suffix"],
      data["bitRate"] as int,
      data["path"],
      data["discNumber"] as int?,
      DateTime.parse(data["created"]),
      data["type"],
    );
  }
}
