class User {
  final String username;
  final bool scrobblingEnabled;
  final bool adminRole;
  final bool settingsRole;
  final bool downloadRole;
  final bool uploadRole;
  final bool playlistRole;
  final bool coverArtRole;
  final bool commentRole;
  final bool podcastRole;
  final bool streamRole;
  final bool jukeboxRole;
  final bool shareRole;
  final bool videoConversionRole;

  User(
      this.username,
      this.scrobblingEnabled,
      this.adminRole,
      this.settingsRole,
      this.downloadRole,
      this.uploadRole,
      this.playlistRole,
      this.coverArtRole,
      this.commentRole,
      this.podcastRole,
      this.streamRole,
      this.jukeboxRole,
      this.shareRole,
      this.videoConversionRole);

  factory User.fromJSON(Map<String, dynamic> data) {
    return User(
        data["username"],
        (data["scrobblingEnabled"] as bool?) ?? false,
        (data["adminRole"] as bool?) ?? false,
        (data["settingsRole"] as bool?) ?? false,
        (data["downloadRole"] as bool?) ?? false,
        (data["uploadRole"] as bool?) ?? false,
        (data["playlistRole"] as bool?) ?? false,
        (data["coverArtRole"] as bool?) ?? false,
        (data["commentRole"] as bool?) ?? false,
        (data["podcastRole"] as bool?) ?? false,
        (data["streamRole"] as bool?) ?? false,
        (data["jukeboxRole"] as bool?) ?? false,
        (data["shareRole"] as bool?) ?? false,
        (data["videoConversionRole"] as bool?) ?? false);
  }

  @override
  String toString() {
    return username;
  }
}
