class MusicFolder {
  final int id;
  final String name;

  const MusicFolder(this.id, this.name);

  factory MusicFolder.fromJSON(Map<String, dynamic> data) {
    return MusicFolder(data["id"] as int, data["name"]);
  }

  @override
  String toString() {
    return "$name($id)";
  }
}
