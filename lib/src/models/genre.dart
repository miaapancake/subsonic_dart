import 'package:subsonic_dart/src/client.dart';
import 'package:subsonic_dart/src/models/song.dart';

class Genre {
  String value;
  int songCount;
  int albumCount;

  Genre(this.value, this.songCount, this.albumCount);

  factory Genre.fromJSON(Map<String, dynamic> data) {
    return Genre(
        data["value"], data["songCount"] as int, data["albumCount"] as int);
  }

  @override
  String toString() {
    return "$value ($songCount songs, $albumCount albums)";
  }
}

class SongsByGenre {
  int _offset = 0;
  final String _genre;

  final int _count;

  final SubSonicClient _client;
  final List<Song> songs;

  SongsByGenre(this._client, this._genre, this._count, this.songs);

  factory SongsByGenre.fromJSON(
      SubSonicClient client, String genre, int count, List<dynamic>? data) {
    return SongsByGenre(client, genre, count,
        data?.map((data) => Song.fromJSON(client, data)).toList() ?? []);
  }

  Future<bool> fetchMore() async {
    _offset += _count;
    final result =
        await _client.getSongsByGenre(_genre, offset: _offset, count: _count);

    if (result.songs.isNotEmpty) {
      songs.addAll(result.songs);
      return true;
    }

    return false;
  }

  @override
  String toString() {
    return "${songs.length} songs";
  }
}
