import 'package:subsonic_dart/src/client.dart';

void main() async {
  var client = SubSonicClient.withAuth("localhost:4533", "test", "test123",
      uriFormat: URIFormat.http);

  // When a successful ping is made, the method should return true
  assert(await client.ping());

  client = SubSonicClient.withAuth("localhost:4533", "invalidtest", "test123",
      uriFormat: URIFormat.http);

  assert(!await client.ping());
}
