import 'package:subsonic_dart/src/client.dart';

void main() async {
  final client = SubSonicClient.withAuth("localhost:4533", "test", "test123",
      uriFormat: URIFormat.http);
  final result = await client.getSongsByGenre("Rock");

  print(result);
  while (await result.fetchMore()) {
    print(result);
  }
}
