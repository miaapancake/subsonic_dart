import 'package:subsonic_dart/src/client.dart';

void main() async {
  final client = SubSonicClient.withAuth(
      "localhost:4533", "test", "test123",
      uriFormat: URIFormat.http);
  final song = await client.getSong("b4d1104323c62e79b7df8dbcf812c771");
  print(song);
  print("\nCover Art Url:");
  print(song.getCoverArtUri());
  print("\nDownload Url:");
  print(song.getDownloadUri());
  print("\nStreaming Url:");
  print(song.getStreamUri());
}
