import 'package:subsonic_dart/src/client.dart';

void main() async {
  final client = SubSonicClient.withAuth(
      "localhost:4533", "test", "test123",
      uriFormat: URIFormat.http);

  print(await client.getAlbumSongs("045bcee728d3f7cdede19453ca18e36b"));
}
