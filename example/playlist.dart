import 'package:subsonic_dart/src/client.dart';

void main() async {
  final client = SubSonicClient.withAuth(
      "localhost:4533", "test", "test123",
      uriFormat: URIFormat.http);

  final playlist = await client.createPlaylist("MyPlaylist");

  await client.updatePlaylist(playlist.id, name: "MyNewPlaylist");

  print(await client.getPlaylist(playlist.id));

  await client.updatePlaylist(playlist.id,
      name: "MyNewPlaylist", songIdToAdd: "b4d1104323c62e79b7df8dbcf812c771");

  print(await client.getPlaylist(playlist.id));
  print(await client.getPlaylistEntries(playlist.id));

  await playlist.removeSong(0);

  print(await client.getPlaylistEntries(playlist.id));

  await client.deletePlaylist(playlist.id);
}
