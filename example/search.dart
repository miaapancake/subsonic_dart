import 'package:subsonic_dart/src/client.dart';

void main() async {
  final client = SubSonicClient.withAuth(
      "localhost:4533", "test", "test123",
      uriFormat: URIFormat.http);
  final result = await client.search("Various");

  print(result);
  while (await result.fetchMoreSongs()) {
    print(result);
  }
}
