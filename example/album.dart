import 'package:subsonic_dart/src/client.dart';

void main() async {
  final client = SubSonicClient.withAuth("localhost:4533", "test", "test123",
      uriFormat: URIFormat.http);

  print(await client.getAlbum("45afa4d7a4775b604c9031fc1ab725cf"));
}
