import 'package:subsonic_dart/src/client.dart';

void main() async {
  final client = SubSonicClient.withAuth("localhost:4533", "test", "test123",
      uriFormat: URIFormat.http);

  final artist = await client.getArtist("0f82983e3dd52ca55e7d21966d437014");

  print(artist);
  print(await artist.getAlbums());
  print(await artist.getSongs());
}
