import 'package:subsonic_dart/src/client.dart';

void main() async {
  assert(await SubSonicClient.testCredentials(
      "localhost:4533", "test", "test123",
      uriFormat: URIFormat.http));

  assert(!await SubSonicClient.testCredentials(
      "localhost:4533", "invalidtest", "test123",
      uriFormat: URIFormat.http));
}
