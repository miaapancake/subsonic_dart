import 'package:subsonic_dart/src/client.dart';

void main() {
  final client = SubSonicClient.withAuth("localhost:4533", "test", "test123",
      uriFormat: URIFormat.http);

  print(client.getAvatarUri());
}
